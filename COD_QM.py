import numpy as np
import os.path
import math

from ase import Atoms
from ase.units import Bohr, Ry, Hartree
from ase.io import espresso

from pymatgen.io.cif import CifParser
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
#from pymatgen.core.structure import Structure
from pymatgen.ext.matproj import MPRester

class COD_QE(object):
    """
    A class to analyse Crystallography Open Database (COD) entries using Quantum Espresso ab-initio calculations. Only projector augmented wave method (PAW) PBE type of pseudopotentials are used. 

    Takes a CIF pathname and creates a pymatgen Structure object which is used by other methods in this class.

    Parameters
    ----------
    pathname: str
        CIF pathname e.g. '/home/user/COD/salts/4344366.cif'
    """

    def __init__(self, pathname):
        self.pathname = pathname
        parsed_cif = CifParser(os.path.join(pathname))
        self.pymatgen_structures = parsed_cif.get_structures()

    def get_pymatgen_structure_object(self):
        """
        Returns
        -------
        object
            pymatgen Structure object.
        """
        return self.pymatgen_structures[0]

    def get_ase_atoms_object(self):
        """
        Returns
        -------
        object
            ase Atoms object.
        """
        return AseAtomsAdaptor.get_atoms(self.pymatgen_structures[0])

    def get_nat(self):
        """
        Returns
        -------
        int
            Number of atoms in cell.
        """
        return len(self.get_ase_atoms_object())

    def get_positions(self):
        """
        Returns
        -------
        numpy array
            Positions of atoms in cell.
        """
        return self.get_ase_atoms_object().get_positions()

    def get_chemical_symbols(self):
        """
        Returns
        -------
        list
            Returns a list of chemical symbols e.g. ['Na', 'Na', 'S'].
        """
        return self.get_ase_atoms_object().get_chemical_symbols()

    def get_element_dictionary(self):
        """
        Returns
        -------
        dict
            Dictionary of element names and number of elements in cell e.g. {'Na': 2, 'S': 1}.
        """
        element_dictionary = {}
        for element in self.get_chemical_symbols():
            element_dictionary[element] = element_dictionary.setdefault(
                element, 0) + 1
        return element_dictionary

    def get_pseudopotentials(self, pseudo_dir):
        """
        Get dictionary of element names and their PBE pseudopotential file names.

        Names of PBE pseudopotential files are taken from the pseudo_dir directory 
        where Quantum Espresso PBE pseudopotential files are stored e.g. '/home/user/QE_psps'.
        Make sure to have only one PBE pseudopotential file for each chemical element in pseudo_dir.

        Parameters
        ----------
        pseudo_dir: str
            Name of directory where PBE pseudopotential files are stored.

        Returns
        -------
        dict
            Dictionary of element names and PBE pseudopotential names e.g. {'Na': 'Na.pbe-spn-kjpaw_psl.1.0.0.UPF', 'S': 'S.pbe-n-kjpaw_psl.1.0.0.UPF'}.
        """
        pseudopotentials = {}
        for key, value in self.get_element_dictionary().items():
            for filename in os.listdir(pseudo_dir):
                if key + '.pbe' in filename:
                    pseudopotentials[key] = filename.replace(
                        str(pseudo_dir)+'/', '')
        return pseudopotentials

    def get_nbnd(self, pseudo_dir):
        """
        Number of electronic states (bands) to be calculated.

        Takes number of valence electrons for each element from pseudopotential file located in pseudo_dir.
        For an insulator, nbnd = number of valence bands (nbnd = # of electrons /2) and for a metal, 20% more (minimum 4 more).

        Parameters
        ----------
        pseudo_dir: str
            Name of directory where PBE pseudopotential files are stored.

        Returns
        -------
        int
            Number of electronic states (bands) used in DFT calculations.
        """
        nelec = 0
        for key, value in self.get_pseudopotentials(pseudo_dir).items():
            pseudo_file = open(os.path.join(
                pseudo_dir, self.get_pseudopotentials(pseudo_dir)[key]), 'r')
            for line in pseudo_file:
                if "z_valence" in line:
                    PP_HEADER = line.split()
                    for item in PP_HEADER:
                        if "z_valence" in item:
                            z_valence = float(item.replace(
                                "z_valence=", "").replace('"', ''))
            nelec += self.get_element_dictionary()[key] * z_valence
            pseudo_file.close()
        nbnd = math.ceil(nelec / 2)  # dividing by 2 and rounding up
        if math.ceil(nbnd * 0.2) <= 4:
            nbnd += 4
        else:
            nbnd += math.ceil(nbnd*0.2)
        return nbnd

    def get_kpoints(self, k_mesh_density):
        """
        Calculates an array that uses it to automatically generate uniform grid of k-points.

        Uses the number of k points of 1000/per atom.
        For insulators, 100 k points/per atom in the full Brillouin zone are generally sufficient to reduce the energy error to less than 10 meV.
        Metals require approximately 1000 k points/per atom for the same accuracy.
        For problematic cases (transition metals with a steep DOS at the Fermi level) it might be necessary to increase the number of k points up to 5000/per atom.

        Bloch vectors (k-points) will be used to sample the Brillouin zone in DFT calculations.
        Automatically generated uniform grid of k-points, i.e, generates Monkhorst-Pack grid [nk1, nk2, nk3].

        Parameters
        ----------
        k_mesh_density: int or 'gamma'
            Number of k points per atom.

        Returns
        -------
        list
            Returns a list that is used to generate a Monkhorst-Pack grid used to sample the Brillouin zone in DFT calculations.
        bool
            Returns None if k_mesh_density = 'gamma'
        """
        if k_mesh_density == 'gamma':
            return None
        else:
            cell = self.get_ase_atoms_object().cell
            reciprocal_cell = cell.reciprocal()
            distances = []
            for reciprocal_vector in reciprocal_cell:
                dist = np.linalg.norm(reciprocal_vector - np.array([0, 0, 0]))
                distances.append(dist)
            distances = np.array(distances)
            norm_distances = np.reciprocal((distances / np.amin(distances)))
            k_mesh = int(k_mesh_density) / self.get_nat()
            daugiklis = (
                k_mesh / (norm_distances[0] * norm_distances[1] * norm_distances[2])) ** (1. / 3.)
            kpts = []
            for value in norm_distances:
                kpts.append(math.ceil(value * daugiklis))
            return kpts

    def write_espresso_in(self, pseudo_dir, calc_type, ecutwfc, nstep, estep, k_mesh_density, max_seconds, restart_mode=None, vdW_corr=None):
        """
        Writes Quantum-Espresso input file filename.pwi to perform geometry optimisation calculations.

        Parameters
        ----------
        pseudo_dir: str
            Name of directory where PBE pseudopotential files are stored.
        calc_type: str
            Quantum Espresso calculation type e.g. 'scf', 'relax', 'vc-relax'
        ecutwfc: int
            Kinetic energy cutoff (Ry) for wavefunctions.
        nstep: int
            Number of structural optimization steps performed in this run.
        estep: int
            Maximum number of iterations in a scf step.
        k_mesh_density: int or 'gamma'
            Number of k points per atom.
        max_seconds: int
            Jobs stop after max_seconds CPU time. Use restart_mode = "restart" to restart an unfinished calculation.
        restart_mode:  str
            If "restart" then restarts from previous interrupted run using same number of CPUs and parallelization.
        vdW_corr: str
            Type of van der Waals correction e.g. "grimme-d2", "grimme-d3", "tkatchenko-scheffler", "many-body dispersion", "XDM"

        Returns
        -------
        file
            Writes a file filename_prefix.pwi in current directory.
        """
        input_data = {
            'control':
                {
                    'calculation': calc_type,
                    'verbosity': 'high',
                    'nstep': int(nstep),
                    'pseudo_dir': os.path.join(pseudo_dir),
                    'outdir': (os.path.basename(self.pathname)).strip('.cif'),
                    'tstress': True,
                    'tprnfor': True,
                    'disk_io': 'nowf',  # save to disk only the xml data file
                    'max_seconds': int(max_seconds)
                },
            'system':
                {
                    'input_dft': 'PBE',
                    'ecutwfc': int(ecutwfc),
                    'ecutrho': int(ecutwfc) * 8,
                    'nbnd': int(self.get_nbnd(pseudo_dir)),
                    'occupations': 'smearing',
                    'degauss': 0.01,
                    'smearing': 'gauss'
                },
            'electrons': {'electron_maxstep': int(estep)}
        }

        if restart_mode is not None:
            input_data['control']['restart_mode'] = restart_mode

        if vdW_corr is not None:
            input_data['system']['vdw_corr'] = vdW_corr

        if calc_type == 'relax':
            input_data['ions'] = {'ion_dynamics': 'bfgs'}
        elif calc_type == 'vc-relax':
            input_data['ions'] = {'ion_dynamics': 'bfgs'}
            input_data['cell'] = {'cell_dynamics': 'bfgs', 'cell_dofree': 'all'}

        with open((os.path.basename(self.pathname)).strip('.cif') + '.pwi', 'w') as f:
            espresso.write_espresso_in(fd=f, atoms=self.get_ase_atoms_object(), pseudopotentials=self.get_pseudopotentials(pseudo_dir),
                                       input_data=input_data, kpts=self.get_kpoints(k_mesh_density), crystal_coordinates=False)

    # def find_materials_project_match(self, MAPI_key):
    #     """
    #     Checks Materials Project database for a matching COD entry structure.

    #     Parameters
    #     ----------
    #     MAPI_key: str
    #         The Materials API key obtained by registering in https://www.materialsproject.org/open

    #     Returns
    #     -------
    #     bool
    #         Returns True if there are more than 1 matching COD structues in Materials Project database.
    #     """
    #     chem_formula = ''
    #     for key, value in self.get_element_dictionary().items():
    #         chem_formula += key + str(value)
    #     #print(chem_formula)

    #     analyze_symmetry = SpacegroupAnalyzer(self.get_pymatgen_structure_object())
    #     spacegroup_number = analyze_symmetry.get_space_group_number()
    #     #print(spacegroup_number)

    #     with MPRester(MAPI_key) as m:
    #         pymatgen_matches = m.get_data(chem_formula)
    #         if len(pymatgen_matches) == 0:
    #                 return print('Found no matching structure.')
    #         for pymatgen_entry in pymatgen_matches:
    #             if pymatgen_entry['spacegroup']['number'] == spacegroup_number:
    #                 #return print(chem_formula, spacegroup_number, '\n', 'Found matching Materials Project entry with id:', pymatgen_entry['material_id'])
    #                 return pymatgen_entry['material_id']
    #             else:
    #                 return print('Found no matching structure.')

    # def compare_relaxed_structure(self, xml_file_location):
    #     COD_structure = self.pymatgen_structures[0]
    #     xml_file = parse(os.path.join(xml_file_location, 'pwscf.xml'))
    #     relaxed_structure = Structure(
    #         xml_file.get_cell(Angstrom=True),
    #         xml_file.get_elements(),
    #         xml_file.get_positions(Angstrom=True))

    #     with MPRester(" ") as m:
    #         pymatgen_id = m.find_structure(COD_structure)
    #         print(pymatgen_id)
    #         pymatgen_structure = m.get_structure_by_material_id(pymatgen_id[0])

    #         COD_structure_lattice = {}
    #         COD_structure_lattice['a'] = COD_structure.as_dict()[
    #             'lattice']['a']
    #         COD_structure_lattice['b'] = COD_structure.as_dict()[
    #             'lattice']['b']
    #         COD_structure_lattice['c'] = COD_structure.as_dict()[
    #             'lattice']['c']
    #         COD_structure_lattice['alpha'] = COD_structure.as_dict()[
    #             'lattice']['alpha']
    #         COD_structure_lattice['beta'] = COD_structure.as_dict()[
    #             'lattice']['beta']
    #         COD_structure_lattice['gamma'] = COD_structure.as_dict()[
    #             'lattice']['gamma']

    #         pymatgen_structure_lattice = {}
    #         pymatgen_structure_lattice['a'] = pymatgen_structure.as_dict()[
    #             'lattice']['a']
    #         pymatgen_structure_lattice['b'] = pymatgen_structure.as_dict()[
    #             'lattice']['b']
    #         pymatgen_structure_lattice['c'] = pymatgen_structure.as_dict()[
    #             'lattice']['c']
    #         pymatgen_structure_lattice['alpha'] = pymatgen_structure.as_dict()[
    #             'lattice']['alpha']
    #         pymatgen_structure_lattice['beta'] = pymatgen_structure.as_dict()[
    #             'lattice']['beta']
    #         pymatgen_structure_lattice['gamma'] = pymatgen_structure.as_dict()[
    #             'lattice']['gamma']

    #         relaxed_structure_lattice = {}
    #         relaxed_structure_lattice['a'] = relaxed_structure.as_dict()[
    #             'lattice']['a']
    #         relaxed_structure_lattice['b'] = relaxed_structure.as_dict()[
    #             'lattice']['b']
    #         relaxed_structure_lattice['c'] = relaxed_structure.as_dict()[
    #             'lattice']['c']
    #         relaxed_structure_lattice['alpha'] = relaxed_structure.as_dict()[
    #             'lattice']['alpha']
    #         relaxed_structure_lattice['beta'] = relaxed_structure.as_dict()[
    #             'lattice']['beta']
    #         relaxed_structure_lattice['gamma'] = relaxed_structure.as_dict()[
    #             'lattice']['gamma']

    #     return {'COD': COD_structure_lattice, 'pymatgen': pymatgen_structure_lattice, 'relaxed': relaxed_structure_lattice}
    #     #analyze_symmetry = SpacegroupAnalyzer(self.pymatgen_structures[0])
    #     # return analyze_symmetry.get_conventional_standard_structure()
