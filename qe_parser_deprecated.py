import numpy as np
import xml.etree.ElementTree as ET
from scipy.spatial import cKDTree
import matplotlib.pyplot as plt

Hartree_unit = 27.21138624598853
Ry_unit = Hartree_unit / 2.0
Bohr_unit = 0.52917721090380

class parse(object):
    """Enter the XML filename to create an object on which to use methods in this class.
    Requires packages: numpy, xml.etree.ElementTree, matplotlib.pyplot"""
    global Hartree_unit
    global Bohr_unit

    def __init__(self, filename):
        self.filename = ET.parse(filename).getroot()  # filename

    def get_etot(self, eV=None):
        """Returns total energy in Hartree units.
        If eV=True, returns in electronvolts."""
        etot = float(self.filename.find('.output/total_energy/etot').text)
        if eV is not None:
            etot = etot * Hartree_unit
        return etot

    def get_nat(self):
        """Returns number of atoms in the system."""
        return int(self.filename.find('.output/atomic_structure').attrib['nat'])

    def get_cell(self, Angstrom=None):
        """Returns a numpy array of cell vectors.
        If Angstrom=None, returns in Bohr radius units.
        Angstrom=True returns in Angstroms."""
        cell = self.filename.find('.output/atomic_structure/cell')
        cell_vectors = []
        if Angstrom is not None:
            for cell_vector in cell:
                # cell vector in angstroms
                cell_vectors.append(
                    [coord * Bohr_unit for coord in list(map(float, cell_vector.text.split()))])
        else:
            for cell_vector in cell:
                # cell vector in Bohr radius units
                cell_vectors.append(list(map(float, cell_vector.text.split())))
        return np.array(cell_vectors)

    def get_nelec(self):
        """Returns number of electrons in the system."""
        return int(float(self.filename.find('.output/band_structure/nelec').text))

    def get_positions(self, Angstrom=None):
        """Returns a numpy array of atom positions x,y,z.
        If Angstrom=None, returns in Bohr radius units.
        Angstrom=True returns in Angstroms."""
        positions = self.filename.find(
            '.output/atomic_structure/atomic_positions')
        position_vectors = []
        if Angstrom is not None:
            for vector in positions:
                position_vectors.append(
                    [coord * Bohr_unit for coord in list(map(float, vector.text.split()))])  # in Angstroms
        else:
            for vector in positions:
                # in Bohr radius units
                position_vectors.append(list(map(float, vector.text.split())))
        return np.array(position_vectors)

    def get_elements(self):
        """Returns a numpy array of element names 
        e.g. ['C', 'C', ..., 'C']"""
        positions = self.filename.find(
            '.output/atomic_structure/atomic_positions')
        elements = []
        for element in positions:
            elements.append(element.attrib['name'])
        return np.array(elements)

    def get_forces(self):
        """Returns a numpy array of force components on each atom. 
        Forces are in Hartree/Bohr units.
        To convert to eV/Angstom, just multiply this array by (Hartree / Bohr)"""
        forces = np.array(list(map(float, self.filename.find(
            '.output/forces').text.split()))).reshape((self.get_nat(), 3))
        return forces

    def get_total_drift(self):
        """Returns a numpy array of total drift. 
        Forces are in Hartree/Bohr units.
        To convert to eV/Angstom, just multiply this array by (Hartree / Bohr)"""
        forces = self.get_forces()
        tot_fx, tot_fy, tot_fz = 0, 0, 0
        for force in forces:
            tot_fx += force[0]
            tot_fy += force[1]
            tot_fz += force[2]
        return np.array([tot_fx, tot_fy, tot_fz])

    def get_nbands(self):
        """Returns a numpy array of numbers of: 
        [ nbands specified in the input, nbands spin up, nbands spin down]."""
        nbnds = int(float(self.filename.find('.input/bands/nbnd').text))
        nbnd_up = int(float(self.filename.find(
            '.output/band_structure/nbnd_up').text))
        nbnd_dw = int(float(self.filename.find(
            '.output/band_structure/nbnd_dw').text))
        return np.array([nbnds, nbnd_up, nbnd_dw])

    def get_eigenvalues(self, eV=None):
        """Returns TWO numpy arrays of Kohn-Sham energy eigenvalues.
        First array is majority spin channel eigenvalues.
        Second array is minority spin channel eigenvalues.
        By default returns in Hartree units.
        If eV=True, returns in electronvolts."""
        nbands = self.get_nbands()
        eigenvalues = list(map(float, self.filename.find(
            './output/band_structure/ks_energies/eigenvalues').text.split()))
        spin_majority = np.array(eigenvalues[0:nbands[1]])
        spin_minority = np.array(eigenvalues[nbands[1]:(nbands[1]+nbands[2])])
        eigenvalues = np.array([spin_majority, spin_minority])
        if eV is not None:
            eigenvalues = [eingenvalue_array *
                           Hartree_unit for eingenvalue_array in eigenvalues]
        return eigenvalues

    def get_occupations(self):
        """Returns TWO numpy arrays of occupations.
        First array is majority spin channel occupations.
        Second array is minority spin channel occupations."""
        nbands = self.get_nbands()
        occupations = list(map(float, self.filename.find(
            './output/band_structure/ks_energies/occupations').text.split()))
        spin_majority = np.array(occupations[0:nbands[1]])
        spin_minority = np.array(occupations[nbands[1]:(nbands[1]+nbands[2])])
        return np.array([spin_majority, spin_minority])

    def band_structure(self, eV=None):
        """Returns dictionary band_parameters with bandgap, bandgap_type, VBM, CBM.
        If eV=True, returns in electronvolts."""
        band_parameters = dict()
        VBM = list()
        CBM = list()
        for ks in self.filename.find('./output/band_structure').findall('ks_energies'):
            occupations = np.array(list(map(float, ks.find('occupations').text.split())))
            evals = np.array(list(map(float, ks.find('eigenvalues').text.split()))) #* Hartree_unit # eV
            for i, occupancy in enumerate(occupations):
                if occupancy < 0.5:
                    VBM.append(evals[i-1])
                    CBM.append(evals[i])
                    break
        if abs(VBM.index(max(VBM)) - CBM.index(min(CBM))) <= 1:
            bandgap_type = str(VBM.index(max(VBM)))+' '+str(CBM.index(min(CBM)))+' direct'
        else:
            bandgap_type = str(VBM.index(max(VBM)))+' '+str(CBM.index(min(CBM)))+' indirect'

        if eV is not None:
            VBM = [value * Hartree_unit for value in VBM]
            CBM = [value * Hartree_unit for value in CBM]

        band_parameters['bandgap'] = min(CBM) - max(VBM)
        band_parameters['bandgap_type'] = bandgap_type
        band_parameters['VBM'] = max(VBM)
        band_parameters['CBM'] = max(CBM)
        return band_parameters
    
    def get_VBM(self, eV=None):
        """Returns value of valence band maximum.
        If eV=True, returns in electronvolts."""
        return self.band_structure(eV)['VBM']

    def get_CBM(self, eV=None):
        """Returns value of conductance band minimum.
        If eV=True, returns in electronvolts."""
        return self.band_structure(eV)['CBM']
        
    def get_bandgap(self, eV=None):
        """Returns value of bandgap.
        If eV=True, returns in electronvolts."""
        return self.band_structure(eV)['bandgap']
    
    def get_bandgap_type(self, eV=None):
        """Returns value of valence band maximum.
        If eV=True, returns in electronvolts."""
        return self.band_structure(eV)['bandgap_type']        
    
    def get_neighbour_distances(self, atom_ID, search_radius, return_ID=None):
        """Returns a numpy array of neighbouring atom distances in a specified search_radius 
        If return_ID=True then reutns a numpy array of neighbouring atom IDs
        Getting the coordinates of neighbours can be done with .get_positions(Angstrom=True)[ID]"""
        positions = self.get_positions(Angstrom=True)
        box = [self.get_cell(Angstrom=True)[0][0], self.get_cell(
            Angstrom=True)[1][1], self.get_cell(Angstrom=True)[2][2]]
        # , np.amin(positions, axis=1), np.amin(positions, axis=2)
        min_values = np.amin(positions, axis=0)
        for coords in positions:
            coords[0], coords[1], coords[2] = coords[0] - \
                min_values[0], coords[1]-min_values[1], coords[2]-min_values[2]
        point_tree = cKDTree(positions, boxsize=box)
        ID_list = point_tree.query_ball_point(
            positions[atom_ID], search_radius)
        #neigh_data = point_tree.data[ID_list]
        ID_out = []
        distances = []
        for neigh_ID in ID_list:
            if neigh_ID != atom_ID:
                dx = abs(positions[atom_ID][0] - positions[neigh_ID][0])
                dy = abs(positions[atom_ID][1] - positions[neigh_ID][1])
                dz = abs(positions[atom_ID][2] - positions[neigh_ID][2])
                if dx > box[0]/2:
                    dx = box[0] - dx
                if dy > box[1]/2:
                    dy = box[1] - dy
                if dz > box[2]/2:
                    dz = box[2] - dz
                dist = np.sqrt(dx**2 + dy**2 + dz**2)
                distances.append(dist)
                ID_out.append(neigh_ID)
        if return_ID is not None:
            return ID_out
        else:
            return np.array(distances)

    def write_poscar(self, crystal_coordinates=None):
        """Writes a VASP POSCAR file into the current directory.
        Writes in angstroms by default.
        !!!!COULDN'T DO DIRECT COORDINATES YET, SO IGNORE THE PARAMETER!!!!
        crystal_coordinates=True to write in direct coordinates"""
        element_dictionary = {}
        for element in self.get_elements():
            element_dictionary[element] = element_dictionary.setdefault(
                element, 0) + 1
        with open('POSCAR', 'w') as f:
            f.write('File with: ')
            for key, value in element_dictionary.items():
                f.write(key + str(value) + ' ')
            f.write('\n    1.00000000\n')
            for cell_vector in self.get_cell(Angstrom=True):
                f.write(' '.join(list(map(str, cell_vector))) + '\n')
            f.write('    ')
            for key, value in element_dictionary.items():
                f.write(key + '   ')
            f.write('\n    ')
            for key, value in element_dictionary.items():
                f.write(str(value) + '   ')
            f.write('\nCartesian\n')
            for atom in self.get_positions(Angstrom=True):
                f.write(' '.join(list(map(str, atom))) + '\n')
        return None

    def plot_defect_levels(self, nband_start, nband_end, eV=None):
        """Plots defect levels.
        Requires the input of range of band numbers:
        e.g. .plot_defect_levels(1021, 1023, eV=True)"""
        occupations = self.get_occupations()
        eigenvalues = self.get_eigenvalues()
        if eV is not None:
            eigenvalues = self.get_eigenvalues(eV=True)
        fig, axes = plt.subplots(1, 2, sharey=True)
        spin_majority_ax = axes[0]
        spin_minority_ax = axes[1]
        estep = 0.15
        for band_no in range(nband_start, nband_end+1):
            spin_majority_ax.hlines(
                eigenvalues[0][band_no], band_no-0.25, band_no+0.25)
            if occupations[0][band_no] == 1.0:
                spin_majority_ax.arrow(
                    band_no, eigenvalues[0][band_no]+estep, 0.0, -2*estep, fc="k", ec="k", head_width=estep/2, head_length=estep/2)
            spin_minority_ax.hlines(
                eigenvalues[1][band_no], band_no-0.25, band_no+0.25)
            if occupations[1][band_no] == 1.0:
                spin_minority_ax.arrow(
                    band_no, eigenvalues[1][band_no]-estep, 0.0, 2*estep, fc="k", ec="k", head_width=estep/2, head_length=estep/2)
            if occupations[0][band_no] == 0.5:
                spin_majority_ax.arrow(
                    band_no, eigenvalues[0][band_no]+estep, 0.0, -2*estep, fc="r", ec="r", head_width=estep/2, head_length=estep/2)
            spin_minority_ax.hlines(
                eigenvalues[1][band_no], band_no-0.25, band_no+0.25)
            if occupations[1][band_no] == 0.5:
                spin_minority_ax.arrow(
                    band_no, eigenvalues[1][band_no]-estep, 0.0, 2*estep, fc="r", ec="r", head_width=estep/2, head_length=estep/2)

        spin_majority_ax.xaxis.set_ticks(
            np.arange(nband_start, nband_end+1, 1.0))
        spin_minority_ax.xaxis.set_ticks(
            np.arange(nband_start, nband_end+1, 1.0))
        #spin_majority_ax.set_ylim([13.5, 17.8])
        #spin_minority_ax.set_ylim([13.5, 17.8])
        plt.show()
        return None