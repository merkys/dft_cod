from COD_QM import COD_QM
import os

CIF_list = ['1527735', '1528545', '1530141', '1535922', '1537335', '1539138', '1539418', '1539744', '1541541', '1541841', '1542071', '1542138', '1542139', '1542197', '2013551', '2207375', '4124784', '4320809', '4344366', '7200689', '7222980', '7222981', '7223666', '8102920', '8103714']
pseudo_dir = '/scratch/vytautas/calculations/COD/qe_psps/PBE'
#pseudo_dir = '/home/vytautas/puntukas/scratch/vytautas/calculations/COD/qe_psps/PBE'

for COD_number in CIF_list[1:]:
    print(COD_number)
    entry = COD_QM(COD_number)
    prefix = ''
    for key, value in entry.get_element_dictionary().items():
        prefix += key + str(value)
    if os.path.isdir(prefix) is False:
        os.system("mkdir " + prefix)
    print(prefix)
    entry.write_espresso_in(prefix, pseudo_dir)
    
    shell_qe_calc_command = 'mpirun -np 36 pw.x -npool 6 -ndiag 36 < ' + prefix + '.pwi > ' + prefix + '.pwo'
    os.system(shell_qe_calc_command)
    os.system('wait')
    
    mv_pw_files = 'mv ' + prefix + '.pw* ' + prefix + '/'
    mv_xml = 'mv pwscf.xml ' + prefix + '/'
    os.system(mv_pw_files)
    os.system(mv_xml)