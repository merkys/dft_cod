import numpy as np
import os
#from qe_parser import parse
from ase.units import Angstrom, Bohr, Ry, Hartree
from pymatgen.core.lattice import Lattice
import xml.etree.ElementTree as ET
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from pymatgen.core.structure import Structure
from ase.io import espresso
from ase import Atoms
from qe_parser import pwscf_xml as parser
from qe_parser_deprecated import parse
from ase.visualize import view

# rootdir = "/home/user/ameba/qe_cod/organics/" # local
rootdir = "/home/vytautas/ameba/qe_cod/inorganics/"  # ameba

COD_ID = '1531358'

try:
    xml = parser(os.path.join(rootdir, COD_ID, 'pwscf.xml'))
except:
    xml = parser(os.path.join(rootdir, COD_ID, 'pwscf.save', 'data-file-schema.xml'))

structures = xml.get_ase_structure_list(Angstrom=True)
view(structures)

# symmetry = SpacegroupAnalyzer(xml.get_pymatgen_structure_list()[-1])
# print(symmetry.get_space_group_symbol())

# #xml = pwscf_xml('/home/user/ameba/qe_cod/metals-and-alloys/1510584/pwscf.xml') # scf yes opt yes
# #xml = pwscf_xml('/home/user/ameba/qe_cod/metals-and-alloys/9012435/pwscf.xml') # scf yes opt no
# #xml = pwscf_xml('/home/user/puntukas/scratch/vytautas/calculations/diamond/QE/qe_pbe/C/kpts/out-ecut-4/pwscf.xml') # scf yes
# #xml = pwscf_xml('/home/user/puntukas/scratch/vytautas/calculations/diamond/QE/qe_pbe/GeV-1/unfinished/out-GeV-1/pwscf.save/data-file-schema.xml') # scf no

# # spin polarised
# #xml = pwscf_xml('/home/user/puntukas/scratch/vytautas/calculations/diamond/NV/NV-1/4x4x4/qe_scan/relax-excited-sym/out-NV-1/pwscf.xml') # scf yes opt yes

# #xml = pwscf_xml('/home/user/ameba/qe_cod/organics/1100459/pwscf.xml')

# # xml.get_convergence_info(print_info=True)