#!/bin/bash

# Script to generate QE input files using Python with cif2qein.py
# Absolute paths will be uncommented when proper python packages will be able to be installed
# Requirements:
# Quantum Espresso >6.6 pw.x
# Python3.6 or newer with packages:
#     os, numpy, ase, pymatgen, math, xml

WORKDIR="organics"
#WORKDIR="/home/vytautas/qe_cod/"
CIF_DIR="$WORKDIR/CIFs/" # Directory where CIF files are located
CIF_FILES="$CIF_DIR*"
PSEUDO_DIR="qe_psps/PBE/"
#PSEUDO_DIR="/home/vytautas/qe_cod/qe_psps/PBE/" #Path to directory with PBE pseudopotentials
CALC_TYPE="vc-relax"  # scf, relax or vc-relax

for CIF_FILE in $CIF_FILES
do
    PREFIX=$CIF_DIR
    QE_PREFIX=${CIF_FILE#$PREFIX}
    QE_PREFIX=${QE_PREFIX%".cif"}    
    
    echo "$QE_PREFIX.pwi"    
    python3.9 cif2qein.py --cif $CIF_FILE --prefix $QE_PREFIX --pseudodir $PSEUDO_DIR --calc-type $CALC_TYPE --outdir $WORKDIR
    sed -i "s@outdir.*@outdir = '$WORKDIR/$QE_PREFIX'@" $WORKDIR/$QE_PREFIX.pwi
    #mpirun -np 36 pw.x < "$WORKDIR/$QE_PREFIX.pwi" > "$WORKDIR/$QE_PREFIX.pwo" & 
    #wait
done
