import os
import argparse

parser = argparse.ArgumentParser(description='CIF file number downloader')

parser.add_argument('-f', '--file', nargs='?', required=True, help='File with entry numbers from COD')
parser.add_argument('-o', '--outdir', nargs='?', required=True, help='where to download these CIF files')

args = vars(parser.parse_args())

with open(args['file']) as f:
    lines = f.readlines()
f.close()

def intconvert(value):
    try:
        int(value)
        return int(value)
    except ValueError:
        pass

for line in lines:
    a = intconvert(line.split()[0])
    if a is not None:
        os.system('wget http://www.crystallography.net/cod/' + line.split()[0] + '.cif -P ' + args['outdir'])