from COD_QM import COD_QE
import argparse
import os

parser = argparse.ArgumentParser(
    description='CIF file to Quantum Espresso input file. Example of use: python3.9 cif2qein.py --cif /COD/salts/4344366.cif --psuedodir /COD/qe_psps/PBE_selected --calc-type vc-relax')

parser.add_argument('--cif', nargs='?', required=True,
                    help='CIF pathname e.g. "/home/user/COD/salts/4344366.cif"')
parser.add_argument('--pseudodir', nargs='?', required=True,
                    help='Directory for QE psudopotential files')
parser.add_argument('--calc-type', nargs='?', default='scf',
                    help='QE calculation type e.g. "scf", "relax", "vc-relax"')
parser.add_argument('--ecutwfc', nargs='?', default=50,
                    help='Kinetic energy cutoff (Ry) for wavefunctions')
parser.add_argument('--nstep', nargs='?', default=50,
                    help='Number of structural optimization steps performed in this run')
parser.add_argument('--estep', nargs='?', default=100,
                    help='Maximum number of iterations in a scf step')
parser.add_argument('--kmesh-density', nargs='?', default=1000,
                    help='Number of k points per atom or "gamma" to perform gamma point calculations')
parser.add_argument('--max-seconds', nargs='?', default=86400,
                    help='Jobs stop after max_seconds CPU time. Use restart_mode = "restart" to restart an unfinished calculation')
parser.add_argument('--restart-mode', nargs='?', default=None,
                    help='If "restart" then restarts from previous interrupted run using same number of CPUs and parallelization')
parser.add_argument('--vdW-corr', nargs='?', default=None,
                    help='Type of van der Waals correction e.g. "grimme-d3", "tkatchenko-scheffler", "many-body dispersion", "XDM"')

args = vars(parser.parse_args())

entry = COD_QE(args['cif'])
entry.write_espresso_in(args['pseudodir'], args['calc_type'], args['ecutwfc'], args['nstep'], args['estep'],
                        args['kmesh_density'], args['max_seconds'], args['restart_mode'], args['vdW_corr'])
