import argparse
import os

parser = argparse.ArgumentParser(description='Open directory of PBE pseudopotentials and pick out the ones with the least amount of valence electrons')
parser.add_argument('-p', '--pseudodir', nargs='?', required=True, help='Directory with psudopotential files')
parser.add_argument('-o', '--outdir', nargs='?', required=True, help='Directory to output chosen pseudopotential files')

args = vars(parser.parse_args())

pseudopotentials = dict()

for filename in os.listdir(args['pseudodir']):
    element = filename.split('.')[0]
    if element in pseudopotentials:
        pseudopotentials[element].append(filename)
    else:
        pseudopotentials[element] = [filename]

for pseudos in pseudopotentials:
    if len(pseudopotentials[pseudos]) == 1:
        os.system('cp ' + args['pseudodir'] + pseudopotentials[pseudos][0] + ' ' + args['outdir'])
    elif len(pseudopotentials[pseudos]) > 1:
        z_valences = list()
        rho_cutoffs = list()
        for filename in pseudopotentials[pseudos]:
            pseudo_file = open(os.path.join(args['pseudodir']+filename), 'r')
            for line in pseudo_file:
                if "z_valence" in line:
                    PP_HEADER = line.split()
                    for item in PP_HEADER:
                        if "z_valence" in item:
                            z_valence = float(item.replace("z_valence=", "").replace('"', ''))
                    for item in PP_HEADER:
                        if "rho_cutoff" in item:
                            rho_cutoff = float(item.replace("rho_cutoff=", "").replace('"', ''))
            z_valences.append(z_valence)
            rho_cutoffs.append(rho_cutoff)
        
        indexes = list()
        for i, z in enumerate(z_valences):
            if z == min(z_valences):
                indexes.append(i)
        
        for i, rho in enumerate(rho_cutoffs):
            if rho == min(rho_cutoffs):
                #print(z_valences[i], rho_cutoffs[i], pseudopotentials[pseudos][i])
                os.system('cp ' + args['pseudodir'] + pseudopotentials[pseudos][i] + ' ' + args['outdir'])